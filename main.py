from multiprocessing import Process, Semaphore, Queue,\
                            Pool, Array, Event, current_process, freeze_support
from multiprocessing.pool import ThreadPool
from threading import Thread, current_thread
from mmap import mmap, ACCESS_READ
import os

import numpy as np
from PIL import Image, ImageFilter

MAX_SIZE_X, MAX_SIZE_Y = 2560, 2560
CHANNELS = 3
TOTAL_PIXELS = MAX_SIZE_X*MAX_SIZE_Y*CHANNELS


class ProgressInfo:
    def __init__(self, img_info, percent):
        self.img_info = img_info
        self.percent = percent


class ImageInfo:
    def __init__(self, file_name, img_name):
        self.file_name = file_name
        self.img_name = img_name


class FileInfo:
    def __init__(self, file, mapped_file):
        self.file = file
        self.mapped_file = mapped_file


class FinishedInfo:
    def __init__(self, img_name, size_x, size_y):
        self.img_name = img_name
        self.size_x = size_x
        self.size_y = size_y


def process_image(img_info, progress_q, finished_q, sh_arr, sem, is_threaded):
    p_name = ''
    if is_threaded:
        p_name = current_thread().name
    else:
        p_name = current_process().name

    print("%s: Start processing image %s" % (p_name, img_info.img_name))
    with open(img_info.file_name, 'rb') as img_file:
        mapped_file = mmap(img_file.fileno(), 0, access=ACCESS_READ)

        img = Image.open(mapped_file)
        print("%s: Start blurring image %s" % (p_name, img_info.img_name))
        progress_q.put(ProgressInfo(img_info, 0))
        blurred_img = img.filter(ImageFilter.GaussianBlur)
        if blurred_img.mode == 'RGBA':
            blurred_img = blurred_img.convert('RGB')
        #blurred_img.show()
        print("%s: Finish blurring image %s" % (p_name, img_info.img_name))
        progress_q.put(ProgressInfo(img_info, 100))

        size_y, size_x = blurred_img.size

        sem.acquire()
        print("%s: Semaphore acquired" % p_name)
        print("%s: Start writing %s to shared memory" % (p_name, img_info.img_name))

        np_sh_arr = np.frombuffer(sh_arr.get_obj())
        np_img = np.asarray(blurred_img)
        pixels_count = np_img.shape[0]*np_img.shape[1]*np_img.shape[2]

        np.copyto(np_sh_arr[:pixels_count], np_img.flatten())
        print("%s: Finished writing %s to shared memory" % (p_name, img_info.img_name))

        finished_q.put(FinishedInfo(img_info.img_name, size_x, size_y))


def print_progress(progress_q):
    while True:
        progress_info = progress_q.get()
        print('File %s (%s) is ready by %d%%'%(progress_info.img_info.file_name,
                                               progress_info.img_info.img_name,
                                               progress_info.percent))


class ImageHandler(Process):
    def __init__(self, files_q: Queue, progress_q, finished_q,
                 sh_arr, sem, num_workers, finish_e: Event, use_threads=True):
        super().__init__()
        self.files_q = files_q
        self.progress_q = progress_q
        self.finished_q = finished_q
        self.sh_arr = sh_arr
        self.sem = sem
        self.num_workers = num_workers
        self.finish_e = finish_e
        self.use_threads = use_threads

    def run(self):
        p_name = current_process().name
        print('%s: Starting handler' % p_name)
        workers = None
        if self.use_threads:
            workers = ThreadPool(processes=self.num_workers)
        else:
            workers = Pool(processes=self.num_workers)  # FIXME: Doesn't work

        while not self.finish_e.is_set():
            img_to_process = self.files_q.get()
            print('%s: Image %s has arrived for processing' % (p_name, img_to_process.img_name))
            res = workers.apply_async(func=process_image,
                                      args=(img_to_process, self.progress_q,
                                            self.finished_q, self.sh_arr,
                                            self.sem, self.use_threads),
                                      error_callback=lambda item: print('ERROR:', item))


def get_images():
    return list(map(lambda img: 'imgs/'+img, os.listdir('imgs')))

if __name__ == '__main__':
    freeze_support()
    print("MAIN: Starting program")
    workers_count = 4

    files_queue = Queue()
    progress_queue = Queue()
    finished_queue = Queue()
    shared_array = Array('d', MAX_SIZE_X*MAX_SIZE_Y*CHANNELS)
    receive_sem = Semaphore(1)
    finish_event = Event()

    opened_files = dict()

    child_p = ImageHandler(files_queue, progress_queue, finished_queue,
                           shared_array, receive_sem, workers_count, finish_event,
                           use_threads=True)

    files_to_process = get_images()*10

    img_num = 1
    for file_name in files_to_process:
        image_file = open(file_name, 'rb+')
        mapped_file = mmap(image_file.fileno(), 0, access=ACCESS_READ)
        opened_files[file_name] = FileInfo(image_file, mapped_file)
        files_queue.put(ImageInfo(file_name, 'img_%d' % img_num))
        img_num += 1

    child_p.start()

    progress_thread = Thread(target=print_progress,
                             args=(progress_queue,))
    progress_thread.start()

    n = len(files_to_process)

    try:
        while True:
            finished_img_info = finished_queue.get()
            print('MAIN: Finished %s (size %dx%d). Final processing' % (finished_img_info.img_name,
                                                                        finished_img_info.size_y,
                                                                        finished_img_info.size_x))

            print("MAIN: Reading %s from shared memory" % finished_img_info.img_name)
            pixels_count = finished_img_info.size_x*finished_img_info.size_y*CHANNELS
            res_arr = np.frombuffer(shared_array.get_obj())[:pixels_count]\
                        .reshape((finished_img_info.size_x, finished_img_info.size_y, CHANNELS))
            print("MAIN: Created numpy view")

            pr_img = Image.fromarray(res_arr.astype(np.uint8, copy=False), "RGB")
            print("MAIN: Saving image %s" % finished_img_info.img_name)
            pr_img.save('output/' + finished_img_info.img_name + '.png', "PNG")
            receive_sem.release()
            print('MAIN: Image saved. Semaphore released')
    except Exception as ex:
        print('ERROR in MAIN:', ex)

    child_p.join()
    progress_thread.join()
    print('MAIN: Closing opened files')
    for img_name in opened_files:
        opened_files[img_name].file.close()
    print('MAIN: Done')
